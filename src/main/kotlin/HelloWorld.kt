import HelloWorld.message

fun main() {
    println(message)
}

object HelloWorld {
    val message = "Hello World"
}