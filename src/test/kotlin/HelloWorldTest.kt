import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class HelloWorldTest {
    @Test
    fun `hello world`() {
        assertEquals("Hello World", HelloWorld.message)
    }
}